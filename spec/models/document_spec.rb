RSpec.describe Document, type: :model do

  context 'validations' do
    it 'validates for file presence' do
      document = Document.new
      expect(document.valid?).to eq(false)
      expect(document.errors.full_messages.join(', ')).to include("Document can't be blank")
    end

    it 'validates for file being csv only' do
      document = Document.new(company: Company.create!(name: 'test'))
      filename = 'valid_sample.csv'
      file = Rails.root.join('spec', 'fixtures', filename)
      document.document.attach(io: File.open(file), filename: filename, content_type: 'text/csv')
      expect(document.valid?).to eq(true)
    end

    it 'validates for file not being of csv type' do
      document = Document.new
      filename = 'invalid_sample.pdf'
      file = Rails.root.join('spec', 'fixtures', filename)
      document.document.attach(io: File.open(file), filename: filename, content_type: 'application/pdf')

      expect(document.valid?).to eq(false)
      expect(document.errors.full_messages.join(', ')).to include('Document is not a CSV')
    end
  end
end
