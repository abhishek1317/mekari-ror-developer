RSpec.describe "employees/index", type: :view do
  before(:each) do
    company = Company.create!(name: 'Company')
    assign(:employees, [
      Employee.create!(
        name: "Name",
        email: "email1@example.com",
        phone: "Phone",
        company: company
      ),
      Employee.create!(
        name: "Name",
        email: "email2@example.com",
        phone: "Phone",
        company: company
      )
    ])
  end

  it "renders a list of employees" do
    render
    assert_select "tr>td", text: "Name", count: 2
    assert_select "tr>td", text: "email1@example.com", count: 1
    assert_select "tr>td", text: "email2@example.com", count: 1
    assert_select "tr>td", text: "Phone", count: 2
  end
end
