RSpec.describe "documents/index", type: :view do
  before(:each) do
    company = Company.create!(name: 'test')
    2.times do
      document = Document.new(company: company)
      filename = 'valid_sample.csv'
      file = Rails.root.join('spec', 'fixtures', filename)
      document.document.attach(io: File.open(file), filename: filename, content_type: 'text/csv')
      document.save!
    end

    assign(:documents, Document.all)
  end

  it "renders a list of documents" do
    render
    assert_select "tr>td", text: /storage/, count: 2
  end
end
