RSpec.describe "policies/index",type: :view do
  before(:each) do
    @company = Company.create!(name: 'company')
    assign(:policies, [
      Policy.create!(
        name: "policy 1",
        company: @company
      ),
      Policy.create!(
        name: "policy 2",
        company: @company
      )
    ])
  end

  it "renders a list of policies" do
    render
    assert_select "tr>td", text: "policy 1", count: 1
    assert_select "tr>td", text: "policy 2", count: 1
  end
end
