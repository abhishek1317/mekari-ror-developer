RSpec.describe BulkImportWorker, type: :worker do

  let!(:company) { Company.create!(name: 'test') }
  let(:filename) { 'valid_sample.csv' }
  let(:file) { Rails.root.join('spec', 'fixtures', filename) }
  let(:valid_upload) do
    document = Document.new(company: company)
    document.document.attach(io: File.open(file), filename: filename, content_type: 'text/csv')
    document.save!
    document
  end
  let(:errored_upload) do
    document = Document.new(company: company)
    file = Rails.root.join('spec', 'fixtures', 'invalid_sample.csv')
    document.document.attach(io: File.open(file), filename: 'invalid_sample.csv', content_type: 'text/csv')
    document.save!
    document
  end

  it "creates employees as in the given csv file" do
    BulkImportWorker.new.perform(valid_upload.id)
    expect(Employee.count).to eq(File.open(file).count - 1) # 1 is header
  end

  it "adds employees and returns errors for failed ones" do
    service_response = BulkImportWorker.new.perform(errored_upload.id)
    expect(Employee.count).to eq(3) # 3 are valid
    expect(service_response[:failed_rows].count).to eq(2) # 2 are valid
  end
end
