# frozen_string_literal: true

require 'csv'

class BulkImportWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  attr_accessor :document, :successful_rows, :failed_rows, :company

  def perform(document_id)
    @document = Document.find(document_id)
    @successful_rows = []
    @failed_rows = []
    @company = document.company

    csv_path = document.document_on_disk
    CSV.foreach(csv_path, headers: true).with_index do |row, index|
      process_data!(row.to_h)
    rescue StandardError => e
      capture_failure(row, e, index)
    end
    { failed_rows: failed_rows } # Trigger mail to someone from here, or do something
  end

  private

  def process_data!(data)
    ActiveRecord::Base.transaction do
      employee = company.employees.find_or_initialize_by(email: data['Email'])
      employee.assign_attributes(name: data['Employee Name'],
                                 phone: data['Phone'],
                                 parent_id: manager_id(data))
      employee.save!
      assign_policies!(employee, data)
      successful_rows << data
    end
  end

  def manager_id(data)
    return nil unless data['Report To'].present?

    company.employees.find_or_initialize_by(email: data['Report To']).id
  end

  def assign_policies!(employee, data)
    policy_names = data['Assigned Policies'].split('|')
    policy_names.each do |name|
      policy = company.policies.find_or_create_by!(name: name)
      employee.policies << policy
    end
  end

  def capture_failure(row, error, index)
    row['Row Number'] = index
    row['Error Reason'] = error.message
    failed_rows << row.to_s
  end
end
