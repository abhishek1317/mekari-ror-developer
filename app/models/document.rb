class Document < ApplicationRecord
  has_one_attached :document
  belongs_to :company

  validates :document,
            attached: true,
            size: { less_than: 10.megabytes , message: 'is not given between size' },
            content_type: { in: 'text/csv', message: 'is not a CSV' }

  after_commit :start_import, on: :create

  def document_on_disk
    ActiveStorage::Blob.service.send(:path_for, document.key) if document.attached?
  end

  def start_import
    BulkImportWorker.perform_async(id)
  end
end
