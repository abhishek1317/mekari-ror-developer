class DocumentsController < ApplicationController
  before_action :set_document, only: [:show, :edit, :update, :destroy]

  # GET /documents
  # GET /documents.json
  def index
    @documents = Document.all
  end

  # GET /documents/new
  def new
    @document = Document.new
  end

  # POST /documents
  # POST /documents.json
  def create
    @document = Document.new(document_params)
    @document.document.attach(params[:document][:document]) if params[:document][:document]

    if @document.save
      redirect_to documents_path, notice: 'Document was successfully created.'
    else
      render :new
    end
    rescue StandardError => _
      render :new
  end

  def document_params
    params.require(:document).permit(:company_id) rescue nil
  end
end
