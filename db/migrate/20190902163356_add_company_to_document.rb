class AddCompanyToDocument < ActiveRecord::Migration[5.2]
  def change
    add_column :documents, :company_id, :integer, foreign_key: true, index: true
  end
end
